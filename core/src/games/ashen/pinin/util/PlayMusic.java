package games.ashen.pinin.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;


/**
 * Created by yoba2 on 29.03.2017.
 */

public class PlayMusic {


    public static final int COUNT_FALL_SOUND = 4;


    public static Music playMusic(String name) {
        Music music = Gdx.audio.newMusic(Gdx.files.internal(name));
        //music.play();
        return music;
    }

    public static void playRandomFall() {
        int index = MathUtils.random.nextInt(COUNT_FALL_SOUND);
        play("sfx/fall_"+index+".ogg");
    }

    public static void play(String name) {
        if (P.get().playSound) {


            Sound sound = P.get().asset.get(name, Sound.class);
            sound.play();
        }
    }

    public static void playLoop(String name) {
        if (P.get().playSound) {

            P.get().asset.get(name,Sound.class).loop();
        }
    }

    public static void stop(String name) {
        if (P.get().playSound) {
            P.get().asset.get(name,Sound.class).stop();
        }
    }


}
