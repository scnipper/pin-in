package games.ashen.pinin.util;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import games.ashen.pinin.util.model.LevelsModel;

public class P {
    public static final float WIDTH = 1080;
    public static final float HEIGHT = 1920;
    public static final float WIDTH_PHIS = 9;
    public static final float HEIGHT_PHIS = 16;

    // физические координаты в мировые
    public static final float P_TO_W = WIDTH / WIDTH_PHIS;
    // мировые в физические
    public static final float W_TO_P = HEIGHT_PHIS / HEIGHT;

    public static P instance;
    public static float WORLD_WIDTH;
    public static float WORLD_HEIGHT;
    public PolygonSpriteBatch rootBatch;
    public World worldPhisics;
    public Array<float[]> cachePhisShapes;
    public BitmapFont font;
    public LevelsModel[] levelModels;
    public Texture dotTexture;
    public ShapeRenderer shape;
    public boolean playSound = true;
    public AssetManager asset;

    public static P get() {
        if (instance == null) {
            instance = new P();
        }
        return instance;
    }


    /**
     * Максимальное значение из массива
     *
     * @param array
     * @return
     */
    public float getMaxFromArray(float[] array) {
        float max = Float.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        return max;
    }

    /**
     * Минимальное значение массива
     *
     * @param array
     * @return
     */
    public float getMinFromArray(float[] array) {
        float min = Float.MAX_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        return min;
    }

}
