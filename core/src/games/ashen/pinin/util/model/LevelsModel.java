package games.ashen.pinin.util.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LevelsModel {

    @SerializedName("level")
    @Expose
    private int level;
    @SerializedName("points")
    @Expose
    private List<Integer> points = null;
    @SerializedName("offset_left")
    @Expose
    private List<Integer> offsetLeft = null;
    @SerializedName("offset_right")
    @Expose
    private List<Integer> offsetRight = null;
    private float time;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<Integer> getPoints() {
        return points;
    }

    public void setPoints(List<Integer> points) {
        this.points = points;
    }

    public List<Integer> getOffsetLeft() {
        return offsetLeft;
    }

    public void setOffsetLeft(List<Integer> offsetLeft) {
        this.offsetLeft = offsetLeft;
    }

    public List<Integer> getOffsetRight() {
        return offsetRight;
    }

    public void setOffsetRight(List<Integer> offsetRight) {
        this.offsetRight = offsetRight;
    }

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }
}