package games.ashen.pinin.util;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class Contacts implements ContactListener {
    @Override
    public void beginContact(Contact contact) {

    }

    @Override
    public void endContact(Contact contact) {
        Body bodyB = contact.getFixtureB().getBody();
        Body bodyA = contact.getFixtureA().getBody();

        Body body = null;

        if(bodyA.getUserData() != null) {
            body = bodyA;
        }

        if(bodyB.getUserData() != null) {
            body = bodyB;
        }

        if (body != null) {
            if(body.isAwake() && body.getLinearVelocity().len() > 0)
                PlayMusic.playRandomFall();
        }

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
