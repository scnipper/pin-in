package games.ashen.pinin.screens;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import games.ashen.pinin.entity.DrawLinesSurface;
import games.ashen.pinin.entity.Level;
import games.ashen.pinin.util.P;
import me.creese.util.display.Display;
import me.creese.util.display.GameView;

public class GameScreen extends GameView {

    private final float[] tmpPointsForTest;
    private final Actor waitActor;
    private final DrawLinesSurface drawLinesSurface;
    private Level currLevel;
    private int startLevel = 1;

    public GameScreen(Display root) {
        super(new ExtendViewport(P.WIDTH, P.HEIGHT), root, P.get().rootBatch);
        createWalls();

        tmpPointsForTest = new float[3];
        drawLinesSurface = new DrawLinesSurface(root);
        addActor(drawLinesSurface);

        waitActor = new Actor();
        addActor(waitActor);


    }



    // Для проверки оборота, по часовой если сумма > 0
    public boolean isClockwise(Array<Vector2> points) {
        int sum = 0;
        for (int i = 0; i < points.size; i++) {
            Vector2 p1 = points.get(i);
            Vector2 p2 = points.get((i + 1) % points.size);
            sum += (p2.x - p1.x) * (p2.y + p1.y);
        }
        return sum >= 0;
    }
    //Проверка действительности треугольника
    public boolean validTriangle(Polygon triangle, Vector2 p1, Vector2 p2, Vector2 p3, Array<Vector2> points) {
        for (Vector2 p : points) {
            if (p != p1 && p != p2 && p != p3 && triangle.contains(p)) {
                return false;
            }
        }
        return true;

    }

    private boolean isDegenerateTriangle(Vector2 p1, Vector2 p2, Vector2 p3) {
        float sideA = p1.dst(p2);
        float sideB = p2.dst(p3);
        float sideC = p1.dst(p3);

        tmpPointsForTest[0] = sideA;
        tmpPointsForTest[1] = sideB;
        tmpPointsForTest[2] = sideC;
        float maxSide = P.get().getMaxFromArray(tmpPointsForTest);

        float p = (sideA + sideB + sideC) / 2;

        return maxSide == p;
    }

    /**
     * треангуляция многоугольника
     */
    public Array<Polygon> triangulatePolygon(Array<Vector2> points) {

        Array<Polygon> triangles = new Array<>();


        Vector2 v1 = new Vector2();
        Vector2 v2 = new Vector2();

        boolean clockwise = isClockwise(points);
        int index = 0;

        while (points.size > 2) { //Циклическая проверка для триангуляции

            Vector2 p1 = points.get((index) % points.size);
            Vector2 p2 = points.get((index + 1) % points.size); //Получаем 3 вершины
            Vector2 p3 = points.get((index + 2) % points.size);

            v1.set(p2.x - p1.x, p2.y - p1.y);
            v2.set(p3.x - p1.x, p3.y - p1.y);
            double cross = v1.crs(v2); //векторное произведение

            Polygon triangle = new Polygon(); // Добавляем вершины в треугольник

            triangle.setVertices(new float[]{p1.x, p1.y, p2.x, p2.y, p3.x, p3.y});

            /*Если значение векторного произведения > 0, то многоугольник строился по часовой
              стрелке, проверяем на возможность существования треугольника */
            if (!clockwise && cross >= 0 &&validTriangle(triangle, p1, p2, p3, points)) {

                points.removeValue(p2, false); //Исключаем вершину
                if (!isDegenerateTriangle(p1, p2, p3)) {
                    triangles.add(triangle); //Строим треугольник
                }

            }else if (clockwise && cross <= 0 && validTriangle(triangle, p1, p2, p3, points)) {
                points.removeValue(p2,false);
                if (!isDegenerateTriangle(p1, p2, p3)) {
                    triangles.add(triangle); //Строим треугольник
                }
            } else {
                index++; //Для перехода на следующие точки
                if(index > points.size) {
                    break;
                }
            }
        }

        //Убираем из массива точки, которые уже построились
        points.clear();
        return triangles;
    }

    /**
     * Следующий уровень
     */
    public void nextLevel() {
        startLevel++;
        if(startLevel > P.get().levelModels.length) {
            startLevel = 1;
        }
        waitActor.addAction(Actions.sequence(Actions.delay(0.5f),Actions.run(this::startGame)));

    }

    /**
     * Начало игры
     */
    public void startGame() {
        if (currLevel != null) {
            currLevel.remove();
        }
        currLevel = new Level(getRoot(), startLevel);
        addActor(currLevel);
    }

    /**
     * Перезапуск уровня
     */
    public void restartLevel() {
        drawLinesSurface.clearChildren();
        drawLinesSurface.setDrawGrid(true);
        drawLinesSurface.clearPoints();
        currLevel.startTimer();
    }

    private void createWalls() {
        BodyDef bodyDef = new BodyDef();

        Body wall = P.get().worldPhisics.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();

        polygonShape.set(new float[]{
                0,0,
                0,P.WORLD_HEIGHT*P.W_TO_P,
                -1*P.W_TO_P,P.WORLD_HEIGHT*P.W_TO_P,
                -1*P.W_TO_P,0
        });
        wall.createFixture(polygonShape,0);

        polygonShape.set(new float[]{
                P.WORLD_WIDTH*P.W_TO_P,0,
                P.WORLD_WIDTH*P.W_TO_P,P.WORLD_HEIGHT*P.W_TO_P,
                (P.WORLD_WIDTH+1)*P.W_TO_P,P.WORLD_HEIGHT*P.W_TO_P,
                (P.WORLD_WIDTH+1)*P.W_TO_P,0
        });
        wall.createFixture(polygonShape,0);

    }

    public Level getCurrLevel() {
        return currLevel;
    }

    @Override
    public void addRoot(Display display) {
        super.addRoot(display);
        if (display != null) {
            startGame();
        }
    }
}
