package games.ashen.pinin.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.google.gson.Gson;


import java.io.Reader;
import java.util.List;

import games.ashen.pinin.PinIn;
import games.ashen.pinin.entity.DrawLinesSurface;
import games.ashen.pinin.entity.Level;
import games.ashen.pinin.util.P;
import games.ashen.pinin.util.model.LevelsModel;
import me.creese.util.display.Display;
import me.creese.util.display.GameView;


/**
 * Created by yoba2 on 09.04.2017.
 */

public class LoadingScreen extends GameView {





    public LoadingScreen(PinIn root) {
        super(new FitViewport(P.WIDTH, P.HEIGHT), root);
        addActor(new LogoDraw(root));


    }

    /**
     * Загрузка уровня из файла
     */
    private void loadLevels() {
        Reader reader = Gdx.files.internal("shapes.json").reader();

        Gson gson = new Gson();

        P.get().levelModels = gson.fromJson(reader, LevelsModel[].class);

        P.get().cachePhisShapes = new Array<>();

        PinIn.OFFSET_TO_SCREEN = (P.WORLD_WIDTH - P.WIDTH)/2;
        for (int i = 0; i < P.get().levelModels.length; i++) {
            LevelsModel model = P.get().levelModels[i];
            List<Integer> points = model.getPoints();
            List<Integer> offsetLeft = model.getOffsetLeft();
            List<Integer> offsetRight = model.getOffsetRight();


            Integer[] pointsReal = points.toArray(new Integer[]{});

            float[] floatsPoints = new float[pointsReal.length];
            for (int j = 0; j < pointsReal.length; j++) {
                floatsPoints[j] = pointsReal[j] * DrawLinesSurface.GRID_SIZE * P.W_TO_P;
            }
            for (int j = 0; j < floatsPoints.length; j+=2) {
                floatsPoints[j] += PinIn.OFFSET_TO_SCREEN * P.W_TO_P;
            }

            for (Integer integer : offsetLeft) {
                floatsPoints[integer * 2] -= Level.OFFSET;
            }
            for (Integer integer : offsetRight) {
                floatsPoints[integer * 2] += Level.OFFSET;
            }
            P.get().cachePhisShapes.add(floatsPoints);

        }

    }

    private void load() {
        P.get().asset = new AssetManager();


        BitmapFontLoader.BitmapFontParameter paramFont = new BitmapFontLoader.BitmapFontParameter();

        paramFont.magFilter = Texture.TextureFilter.Linear;
        paramFont.minFilter = Texture.TextureFilter.Linear;


        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();

        P.get().shape = new ShapeRenderer();
        P.get().dotTexture = new Texture(pixmap);
        pixmap.dispose();

        P.get().font = new BitmapFont();
        ExtendViewport extendViewport = new ExtendViewport(P.WIDTH, P.HEIGHT);
        extendViewport.update(Gdx.graphics.getWidth(), Gdx.app.getGraphics().getHeight(), true);
        P.WORLD_WIDTH = extendViewport.getWorldWidth();
        P.WORLD_HEIGHT = extendViewport.getWorldHeight();

        loadLevels();

        P.get().asset.load("sfx/fall_0.ogg",Sound.class);
        P.get().asset.load("sfx/fall_1.ogg",Sound.class);
        P.get().asset.load("sfx/fall_2.ogg",Sound.class);
        P.get().asset.load("sfx/fall_3.ogg",Sound.class);




    }




    @Override
    public void addRoot(Display display) {
        super.addRoot(display);
        if (display != null) {
            load();
        }
    }

    class LogoDraw extends Group {


        private final Sprite splash;
        private final PinIn root;


        private boolean drawBar;

        LogoDraw(PinIn root) {
            this.root = root;
            Texture texture = new Texture("splash/splash.png");
            texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            splash = new Sprite(texture);

            SequenceAction sequence = new SequenceAction();
            sequence.addAction(Actions.delay(0.3f));
            sequence.addAction(Actions.color(Color.WHITE, 0.5f));

            addAction(sequence);

            setColor(new Color(1, 1, 1, 0));
            //splash.setColor(getColor());
            splash.setPosition(P.WIDTH / 2 - (splash.getWidth() / 2), P.HEIGHT / 2 - (splash.getHeight() / 2));
            ///splash.setColor(P.TOP_MENU_COLOR);
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            splash.setColor(getColor());
            splash.draw(batch);
        }

        @Override
        public void act(float delta) {
            super.act(delta);
            try {
                if (P.get().asset.update()) {

                    if (getActions().size == 0 && !drawBar) {
                        addAction(Actions.color(new Color(1, 1, 1, 0), 1));
                        drawBar = true;

                    }

                }
            } catch (GdxRuntimeException e) {
                e.printStackTrace();
            }

            if (drawBar && getActions().size == 0) {
                root.loadOk();
                remove();
                splash.getTexture().dispose();
            }
        }

    }

}
