package games.ashen.pinin.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;

import java.util.List;

import games.ashen.pinin.screens.GameScreen;
import games.ashen.pinin.util.FontUtil;
import games.ashen.pinin.util.P;
import games.ashen.pinin.util.model.LevelsModel;
import me.creese.util.display.Display;

public class Level extends Actor {

    public static final float OFFSET = 2*P.W_TO_P;
    private final Display root;
    private final int num;

    private Body bodyLevel;
    private float[] points;
    private float area;

    private float endPos;
    private Array<Polygon> polygonsForDraw;
    private LevelsModel levelModel;
    private float time;
    private float timeTo;

    public Level(Display root, int num) {
        this.root = root;
        this.num = num;
        loadLevel(num);


        setWidth(P.WORLD_WIDTH);


    }

    private void loadLevel(int num) {
        points = P.get().cachePhisShapes.get(num - 1);

        levelModel = P.get().levelModels[num - 1];
        time = levelModel.getTime();
        float[] yPos = new float[points.length / 2];

        for (int i = 1,j=0; i < points.length; i+=2,j++) {
            yPos[j] = points[i];
        }

        endPos = (int) (P.get().getMinFromArray(yPos)*P.P_TO_W);

        setHeight(P.get().getMaxFromArray(yPos)*P.P_TO_W);

        calcArea();
        float[] pointsCur = new float[points.length + 8];

        pointsCur[0] = 0;
        pointsCur[1] = 0;
        pointsCur[2] = 0;
        pointsCur[3] = points[1];


        pointsCur[pointsCur.length-1] = 0;
        pointsCur[pointsCur.length-2] = P.WORLD_WIDTH*P.W_TO_P;
        pointsCur[pointsCur.length-3] = points[points.length-1];
        pointsCur[pointsCur.length-4] = P.WORLD_WIDTH*P.W_TO_P;

        System.arraycopy(points,0,pointsCur,4,points.length);

        float[] pointsForDraw = new float[pointsCur.length];
        System.arraycopy(pointsCur,0,pointsForDraw,0,pointsCur.length);

        createTrianglesForDraw(pointsForDraw);



        BodyDef bodyDef = new BodyDef();
        bodyLevel = P.get().worldPhisics.createBody(bodyDef);

        Array<Vector2> p = new Array<>();
        for (int i = 0; i < pointsCur.length; i+=2) {
            p.add(new Vector2(pointsCur[i],pointsCur[i+1]));
        }

        GameScreen gameScreen = root.getGameViewForName(GameScreen.class);
        Array<Polygon> polygons = gameScreen.triangulatePolygon(p);


        PolygonShape polygonShape = new PolygonShape();


        for (Polygon polygon : polygons) {
            polygonShape.set(polygon.getVertices());
            bodyLevel.createFixture(polygonShape,0);
        }



        startTimer();


    }

    public void startTimer() {
        getActions().clear();
        timeTo = time;
        addAction(Actions.parallel(Actions.sequence(Actions.delay(time),Actions.run(new Runnable() {
            @Override
            public void run() {
                System.out.println("restart level");
                GameScreen gameScreen = root.getGameViewForName(GameScreen.class);
                gameScreen.restartLevel();
            }
        })),Actions.forever(Actions.run(new Runnable() {
            @Override
            public void run() {
                timeTo -= Gdx.graphics.getDeltaTime();

                if(timeTo < 0) timeTo = 0;

                timeTo = ((int) (timeTo * 100));

                timeTo /= 100.f;


                System.out.println(timeTo);
            }
        }))));
    }

    /**
     * Создаем треугольники для рисования
     * @param pointsForDraw
     */
    private void createTrianglesForDraw(float[] pointsForDraw) {
        LevelsModel levelModel = P.get().levelModels[num - 1];
        List<Integer> offsetLeft = levelModel.getOffsetLeft();
        List<Integer> offsetRight = levelModel.getOffsetRight();
        for (Integer integer : offsetLeft) {
            pointsForDraw[(integer + 2) * 2] += Level.OFFSET;
        }
        for (Integer integer : offsetRight) {
            pointsForDraw[(integer + 2) * 2] -= Level.OFFSET;
        }

        Array<Vector2> p = new Array<>();
        for (int i = 0; i < pointsForDraw.length; i+=2) {
            p.add(new Vector2(pointsForDraw[i]*P.P_TO_W,pointsForDraw[i+1]*P.P_TO_W+2));
        }

        polygonsForDraw = root.getGameViewForName(GameScreen.class).triangulatePolygon(p);

    }

    /**
     * Вычисляем площадь
     */
    private void calcArea() {
        Array<Vector2> p = new Array<>();
        for (int i = 0; i < points.length; i+=2) {
            p.add(new Vector2(points[i],points[i+1]));
        }

        Array<Polygon> polygons = root.getGameViewForName(GameScreen.class).triangulatePolygon(p);

        for (Polygon polygon : polygons) {
            area+= polygon.area();
        }
    }

    public float[] getPoints() {
        return points;
    }

    public float getEndPos() {
        return endPos;
    }

    public float getArea() {
        return area;
    }

    @Override
    protected void setParent(Group parent) {
        super.setParent(parent);
        if (parent != null) {

        } else {
            if (bodyLevel != null) {
                P.get().worldPhisics.destroyBody(bodyLevel);
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        FontUtil.drawText(batch,P.get().font,"Level - "+num,0,P.WORLD_HEIGHT-100,4, Color.WHITE,P.WORLD_WIDTH, Align.center);
        FontUtil.drawText(batch,P.get().font,"Left time: "+timeTo,0,P.WORLD_HEIGHT-160,4, Color.RED,P.WORLD_WIDTH, Align.center);

        batch.end();
        P.get().shape.setColor(Color.SKY);
        P.get().shape.begin(ShapeRenderer.ShapeType.Filled);
        P.get().shape.setProjectionMatrix(batch.getProjectionMatrix());
        for (Polygon polygon : polygonsForDraw) {
            float[] vertices = polygon.getVertices();
            P.get().shape.triangle(vertices[0],vertices[1],
                    vertices[2],vertices[3],
                    vertices[4],vertices[5]);
        }
        P.get().shape.end();
        P.get().shape.setColor(Color.WHITE);
        batch.begin();
    }
}
