package games.ashen.pinin.entity;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;

import games.ashen.pinin.screens.GameScreen;
import games.ashen.pinin.util.P;

public class PhysicsShape extends Actor {

    private final Sprite sprite;
    private Body body;
    private Array<Polygon> triangles;
    private Level currLevel;
    private boolean isCompare;
    private float area;
    private int lengthPoints;

    public PhysicsShape(TextureRegion texture) {
        sprite = new Sprite(texture);
        sprite.setOrigin(0,0);
    }

    /**
     * Создание физического тела
     */
    private void createBody() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(getX() * P.W_TO_P, getY() * P.W_TO_P);
        body = P.get().worldPhisics.createBody(bodyDef);


        body.setUserData(this);

        PolygonShape polygonShape = new PolygonShape();
        for (Polygon triangle : triangles) {

            triangle.setScale(P.W_TO_P,P.W_TO_P);
            area+= triangle.area();
            float[] transformedVertices = triangle.getTransformedVertices();
            polygonShape.set(transformedVertices);
            body.createFixture(polygonShape, 0.3f);
        }
        area = Math.abs(area);

    }

    /**
     * Проверяем на конец игры или следующий уровень
     */
    private void comparePoints() {

        if(isCompare) return;

        float areaLevel = Math.abs(currLevel.getArea());
        float offset = 0.5f;
        if(this.area >= areaLevel - offset && this.area <= areaLevel + offset) {
            DrawLinesSurface parent = (DrawLinesSurface) getParent();
            parent.setDrawGrid(true);
            parent.getRoot().getGameViewForName(GameScreen.class).nextLevel();
            remove();
        } else {
            gameOver();
        }



        isCompare = true;
    }

    private void gameOver() {
        ((DrawLinesSurface) getParent()).getRoot().getGameViewForName(GameScreen.class).restartLevel();
    }

    public void setTriangles(Array<Polygon> triangles) {
        this.triangles = triangles;
    }


    public void setLengthPoints(int lengthPoints) {
        this.lengthPoints = lengthPoints;
    }

    @Override
    protected void setParent(Group parent) {
        super.setParent(parent);
        if (parent != null) {
            DrawLinesSurface drawLinesSurface = (DrawLinesSurface) getParent();

            currLevel = drawLinesSurface.getRoot().getGameViewForName(GameScreen.class).getCurrLevel();
            createBody();
        } else {
            sprite.getTexture().dispose();
            if (body != null) {
                P.get().worldPhisics.destroyBody(body);
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Vector2 position = body.getPosition();
        sprite.setPosition(position.x * P.P_TO_W, position.y * P.P_TO_W);
        sprite.setRotation(body.getAngle()* MathUtils.radDeg);
        sprite.draw(batch);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        float endPos = currLevel.getEndPos();
        if(sprite.getY() <= endPos+5 && sprite.getY() >= endPos-5
                && lengthPoints == currLevel.getPoints().length/2
                && !body.isAwake()) {
            comparePoints();
        } else if(!body.isAwake()) {
            gameOver();
        }


    }
}
