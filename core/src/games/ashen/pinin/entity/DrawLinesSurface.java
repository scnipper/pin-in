package games.ashen.pinin.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ScreenUtils;

import games.ashen.pinin.PinIn;
import games.ashen.pinin.screens.GameScreen;
import games.ashen.pinin.util.P;
import games.ashen.pinin.util.Shapes;
import me.creese.util.display.Display;

public class DrawLinesSurface extends Group {
    public static final int GRID_SIZE = 100;

    private final Shapes shapes;
    private final FrameBuffer frameBuffer;
    private final Rectangle rectAABB;
    private final Display root;
    private final int startIndexDrawGrid;

    private float[] pointsX;
    private float[] pointsY;
    private int posArr;
    private boolean isClose;
    private boolean isDrawGrid = true;

    public DrawLinesSurface(Display root) {
        this.root = root;
        setBounds(0, 0, P.WORLD_WIDTH, P.WORLD_HEIGHT);

        shapes = new Shapes();

        pointsX = new float[1];
        pointsY = new float[1];

        startIndexDrawGrid = (int) (PinIn.OFFSET_TO_SCREEN/GRID_SIZE * -1)-1;

        posArr = 0;

        rectAABB = new Rectangle();

        frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, (int) P.WORLD_WIDTH, (int) P.WORLD_HEIGHT, false);


        addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button) {

                int xPoint = Math.round((event.getStageX() - PinIn.OFFSET_TO_SCREEN) / GRID_SIZE);
                int yPoint = Math.round(event.getStageY() / GRID_SIZE);
                addPoint(xPoint * GRID_SIZE, yPoint * GRID_SIZE);

                generateAABBRect();
                if ((xPoint * GRID_SIZE == pointsX[0]- PinIn.OFFSET_TO_SCREEN && yPoint * GRID_SIZE == pointsY[0]) && posArr > 1) {
                    isDrawGrid = false;
                    isClose = true;
                }
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (isClose) {
                    offsetPoints();
                    Array<Vector2> p = new Array<>();
                    GameScreen gameScreen = root.getGameViewForName(GameScreen.class);
                    for (int i = 0; i < pointsX.length - 1; i++) {
                        p.add(new Vector2(pointsX[i], pointsY[i]));
                    }
                    Array<Polygon> triangles = gameScreen.triangulatePolygon(p);

                    frameBuffer.begin();
                    Gdx.gl.glClearColor(1, 1, 1, 0);
                    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

                    P.get().shape.setColor(Color.BLUE);
                    P.get().shape.begin(ShapeRenderer.ShapeType.Filled);
                    P.get().shape.setProjectionMatrix(getStage().getCamera().combined);
                    for (Polygon polygon : triangles) {
                        float[] vertices = polygon.getVertices();
                        P.get().shape.triangle(vertices[0],vertices[1],
                                vertices[2],vertices[3],
                                vertices[4],vertices[5]);
                    }
                    P.get().shape.end();
                    P.get().shape.setColor(Color.WHITE);

                    TextureRegion frameBufferTexture = ScreenUtils.getFrameBufferTexture(0, 0, (int) rectAABB.width, (int) rectAABB.height);

                    frameBuffer.end();

                    PhysicsShape physicsShape = new PhysicsShape(frameBufferTexture);

                    physicsShape.setBounds(rectAABB.x, rectAABB.y, rectAABB.width, rectAABB.height);



                    physicsShape.setLengthPoints(pointsX.length-1);
                    physicsShape.setTriangles(triangles);

                    addActor(physicsShape);

                    gameScreen.getCurrLevel().getActions().clear();
                    clearPoints();


                }
            }

        });
    }

    /**
     * Удаляем все линии
     */
    public void clearPoints() {
        isClose = false;
        pointsX = new float[1];
        pointsY = new float[1];
        posArr = 0;
    }

    private void offsetPoints() {
        float minX = P.get().getMinFromArray(pointsX);
        float minY = P.get().getMinFromArray(pointsY);

        for (int i = 0; i < pointsX.length; i++) {
            pointsX[i] -= minX;
            pointsY[i] -= minY;
        }
    }

    /**
     * Добавляем точку
     *
     * @param x
     * @param y
     */
    private void addPoint(float x, float y) {
        Level currLevel = root.getGameViewForName(GameScreen.class).getCurrLevel();
        if (posArr == pointsX.length && y > currLevel.getHeight()) {
            float[] tmpX = new float[pointsX.length + 1];
            float[] tmpY = new float[pointsY.length + 1];

            System.arraycopy(pointsX, 0, tmpX, 0, pointsX.length);
            System.arraycopy(pointsY, 0, tmpY, 0, pointsY.length);

            pointsX = tmpX;
            pointsY = tmpY;

        }
        if(y > currLevel.getHeight()) {
            pointsX[posArr] = x + PinIn.OFFSET_TO_SCREEN;
            pointsY[posArr] = y;
            posArr++;
        }


    }

    /**
     * Прямоугольник который описывает машину
     */
    private void generateAABBRect() {

        float minX = P.get().getMinFromArray(pointsX);
        float minY = P.get().getMinFromArray(pointsY);
        float maxX = P.get().getMaxFromArray(pointsX);
        float maxY = P.get().getMaxFromArray(pointsY);

        rectAABB.set(minX, minY, maxX - minX, maxY - minY);


    }



    /**
     * Рисуем линии
     */
    private void drawLines() {

        shapes.setColor(Color.RED);
        for (int i = 0; i < pointsX.length - 1; i++) {

            float _x = pointsX[i + 1];
            float _y = pointsY[i + 1];
            shapes.line(this.pointsX[i], pointsY[i], _x, _y, 10);
        }

        //shapes.rectRoundLine(rectAABB.x, rectAABB.y, rectAABB.width, rectAABB.height, 1, 3);
        shapes.flush();
    }

    /**
     * Рисование сетки
     */
    private void drawGrid() {
        shapes.setProjMatrix(getStage().getCamera().combined);
        if (!isDrawGrid) return;

        Level currLevel = root.getGameViewForName(GameScreen.class).getCurrLevel();
        int wCount = (int) (P.WORLD_WIDTH / GRID_SIZE);
        int hCount = (int) ((P.WORLD_HEIGHT)/ GRID_SIZE);

        shapes.setColor(Color.DARK_GRAY);
        for (int i = startIndexDrawGrid; i < wCount; i++) {
            shapes.line((GRID_SIZE + (i * GRID_SIZE))+ PinIn.OFFSET_TO_SCREEN, currLevel.getHeight()+GRID_SIZE,
                    (GRID_SIZE + (i * GRID_SIZE))+ PinIn.OFFSET_TO_SCREEN, P.WORLD_HEIGHT, 5);
        }

        for (int i = 0; i < hCount; i++) {
            shapes.line(0, currLevel.getHeight()+GRID_SIZE + (i * GRID_SIZE), P.WORLD_WIDTH, currLevel.getHeight()+GRID_SIZE + (i * GRID_SIZE), 5);
        }
        for (int i = startIndexDrawGrid; i < wCount; i++) {
            for (int j = 0; j < hCount; j++) {
                shapes.circle((GRID_SIZE + (GRID_SIZE * i))+ PinIn.OFFSET_TO_SCREEN, currLevel.getHeight()+GRID_SIZE + (GRID_SIZE * j), 13, true);
            }
        }

        for (int i = 0; i < pointsX.length; i++) {
            shapes.circleLine((pointsX[i] - 35f), pointsY[i] - 35f, 35, 10);
        }
    }

    public void setDrawGrid(boolean drawGrid) {
        isDrawGrid = drawGrid;
    }

    public Display getRoot() {
        return root;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {


        batch.end();
        drawGrid();
        drawLines();
        batch.begin();

        super.draw(batch, parentAlpha);
    }
}
