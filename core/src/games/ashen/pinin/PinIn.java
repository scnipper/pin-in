package games.ashen.pinin;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.google.gson.Gson;

import java.io.Reader;
import java.util.List;

import games.ashen.pinin.entity.DrawLinesSurface;
import games.ashen.pinin.entity.Level;
import games.ashen.pinin.screens.GameScreen;
import games.ashen.pinin.screens.LoadingScreen;
import games.ashen.pinin.util.Contacts;
import games.ashen.pinin.util.P;
import games.ashen.pinin.util.model.LevelsModel;
import me.creese.util.display.Display;

public class PinIn extends Display {
    private Box2DDebugRenderer debugRend;
    private Camera debugCamera;
    public static float OFFSET_TO_SCREEN;

    @Override
    public void create() {
        setBackgroundColor(Color.LIGHT_GRAY);



        initPhisics();
        P.get().rootBatch = new PolygonSpriteBatch();


        addListGameViews(new LoadingScreen(this));

        showGameView(LoadingScreen.class);
    }

    public void loadOk() {
        addListGameViews(new GameScreen(this));
        showGameView(GameScreen.class);
    }


    /**
     * Инициализация физики
     */
    private void initPhisics() {
        ExtendViewport extendViewport = new ExtendViewport(P.WIDTH_PHIS, P.HEIGHT_PHIS);
        extendViewport.update(Gdx.graphics.getWidth(), Gdx.app.getGraphics().getHeight(), true);

        debugCamera = extendViewport.getCamera();
        P.get().worldPhisics = new World(new Vector2(0, -13), true);

        P.get().worldPhisics.setContactListener(new Contacts());
        debugRend = new Box2DDebugRenderer(true, true, false, true, true, true);
    }

    @Override
    public void render() {
        P.get().worldPhisics.step(0.016666668f, 4, 4);
        super.render();
        //debugRend.render(P.get().worldPhisics, debugCamera.combined);
    }
}
